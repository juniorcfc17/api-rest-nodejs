#!/bin/bash

# Verifique se o nome da migration foi fornecido como argumento
if [ -z "$1" ]; then
  echo "Por favor, forneça o nome da migration como argumento."
  exit 1
fi

# Comando para criar a migration usando pnpm e knex
pnpm run knex -- migrate:make "$1"
